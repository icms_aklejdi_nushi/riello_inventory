jQuery.sap.declare("model.items");
jQuery.sap.require("utils.LocalStorage");

model.items =
{
  items:[],

  getModel: function()
  {

    this.model = new sap.ui.model.json.JSONModel();
    this.getItems();
    this.model.setData(this);
    return this.model;
  },

  _getItemsFromLocalStorage:function()
  {
    this.items = utils.LocalStorage.getItem("itemSet");
  },
 
  getItems:function()
  {
    if(!this.items || (this.items.length === 0))
    this._getItemsFromLocalStorage();
  }
    

};
