jQuery.sap.declare("icms.Component");
jQuery.sap.require("icms.MyRouter");
jQuery.sap.require("icms.model.testUser");
jQuery.sap.require("icms.model.user");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("icms.utils.LocalStorage");
jQuery.sap.require("icms.model.items");




sap.ui.core.UIComponent.extend("icms.Component", {
    metadata: {
        config: {
            resourceBundle: "custom/i18n/text.properties",

            //puntati dalla classe settings.core [custom/settins]
            settings: {
                lang: "IT",
                isMock: true,
                serverUrl: "sap.blabla:8000"
            },
        },
        includes: [ //importare css di custom e lib
            "libs/offline.min.js",
			"css/custom.css",
			"libs/lodash.js",
			"libs/q.js",
			],

        dependencies: {
            libs: [
				"sap.m",
				"sap.ui.layout"
				]
        },

        routing: {
            config: {
                viewType: "XML",
                viewPath: "view",
                clearTarget: false,
                targetControl: "app",
                transition: "flip",
                targetAggregation: "pages",
            },
            routes: [
                {
                    name: "login",
                    pattern: "",
                    view: "Login",
                    viewId: "loginId",
                },
                {
                    name: "home",
                    pattern: "home",
                    view: "Home",
                    viewId: "homeId",
				},
                {
                    name: "detail",
                    //pattern: "detail/{userID}",
                    pattern: "detail",
                    view: "Detail",
                    viewId: "detailId",
				}
  
               
			]
        }
    },

    /**
     * !!! The steps in here are sequence dependent !!!
     */
    init: function () {

        //il ns icms va cambiato a seconda del nome app e nome cliente
        jQuery.sap.registerModulePath("icms", "custom");
        jQuery.sap.registerModulePath("view", "custom/view");
        jQuery.sap.registerModulePath("model", "custom/model");
        jQuery.sap.registerModulePath("utils", "custom/utils");

        // 2. call overridden init (calls createContent)
        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        var oI18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl: [this.getMetadata().getConfig().resourceBundle]
        });
        this.setModel(oI18nModel, "i18n");

        // 3a. monkey patch the router
        jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
        var router = this.getRouter();
        router.myNavBack = icms.MyRouter.myNavBack;
        router.myNavToWithoutHash = icms.MyRouter.myNavToWithoutHash;
        icms.MyRouter.router = router;

        this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);


        // set device model
        var oDeviceModel = new sap.ui.model.json.JSONModel({
            isTouch: sap.ui.Device.support.touch,
            isNoTouch: !sap.ui.Device.support.touch,
            isPhone: sap.ui.Device.system.phone,
            isNoPhone: !sap.ui.Device.system.phone,
            listMode: (sap.ui.Device.system.phone) ? "None" : "SingleSelectMaster",
            listItemType: (sap.ui.Device.system.phone) ? "Active" : "Inactive"
        });
        oDeviceModel.setDefaultBindingMode("OneWay");
        this.setModel(oDeviceModel, "device");


        //setto valori in localstorage
        //var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);

        var appStatusModel = new sap.ui.model.json.JSONModel();
        this.setModel(appStatusModel, "appStatus");
        
        var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);

        var appModel = new sap.ui.model.json.JSONModel({
            user: model.testUser,
            editable: false
        });
        appModel.setDefaultBindingMode("TwoWay");
        this.setModel(appModel, "appModel");

        //**
        
        //this.getRouter().initialize();
        
        router.initialize();
    },

    destroy: function () {
        if (this.routeHandler) {
            this.routeHandler.destroy();
        }

        // call overridden destroy
        sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
    },

    createContent: function () {
        // create root view
        var oView = sap.ui.view({
            id: "mainApp",
            viewName: "view.App",
            type: "JS",
            viewData: {
                component: this
            }
        });

        return oView;
    }
});
