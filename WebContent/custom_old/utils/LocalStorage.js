jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("model.user");
jQuery.sap.declare("utils.LocalStorage");


utils.LocalStorage = {
    _userID: undefined,
    _localStorage: undefined,

    _createPath: function (entitySet) {
        if (!this._localStorage) {
            this._localStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
        };
        this._userID = model.user.getUserID();
        var itemPath = entitySet + "_" + this._userID;
        return itemPath;
    },
    
    getItem: function (entitySet) {
        var itemPath = this._createPath(entitySet);
        return this._localStorage.get(itemPath);
    },
    
    setItem: function (entitySet, value) {
        var itemPath = this._createPath(entitySet);
        return this._localStorage.put(itemPath, value);
    }
};
