jQuery.sap.require("sap.m.MessageBox");
//jQuery.sap.require("sap.ui.core.util.ExportType");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.util");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("utils.AbstractController");
jQuery.sap.require("utils.LocalStorage");
jQuery.sap.require("model.items");

utils.AbstractController.extend("view.Detail", {

  onInit: function () {

    utils.AbstractController.prototype.onInit.apply( this, arguments );
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

    this.itemsModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.itemsModel, "items");

    this.currentUserModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.currentUserModel, "currentUser");
      
    this.onlineModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.onlineModel, "online");

  },

  onExit: function () {

  },

  handleRouteMatched: function (evt) {

    var name = evt.getParameter("name");
    if (name !== "detail") {
      return;
    }
    
    if (sessionStorage.getItem("userID")) {
            //sessionStorage.setItem("noRefresh", true);
            this.userID = sessionStorage.userID;
           
    };
      
    this.currentUserModel.setProperty("/userID", this.userID);
    this.currentUserModel.refresh(true);
      
    if(!sessionStorage.getItem("userID")){
            //this.router.navTo("");
            //this.router.destroy();
            this.currentUserModel.setProperty("/userID", "");
            this.currentUserModel.refresh(true);
    }
      
    Offline.options = {
            checks: {
                xhr: {
                    url:'https://www.google.it'
                    //url: 'http://54.93.218.6:8000/IoT/app/RielloProject/WebContent/custom/image/favicon.ico'
                }
            }
        };
    Offline.check();
    if (Offline.state === "up") {
        this.onlineModel.setProperty("/visibileOnline", false);
    } else if (Offline.state === "down") {
        this.onlineModel.setProperty("/visibileOnline", true);
    }
    this.onlineModel.refresh();
      
    var that = this;
    var funzioneOnline = function () {
        Offline.check();
        if (Offline.state === "up") {
            that.onlineModel.setProperty("/visibileOnline", false);
        } else if (Offline.state === "down") {
            that.onlineModel.setProperty("/visibileOnline", true);
        };
        that.onlineModel.refresh();
    };

    this.mySetInterval = setInterval(funzioneOnline, 30000);
      
    this.itemSet = utils.LocalStorage.getItem("itemSet");
        if(this.itemSet === null || this.itemSet === undefined){
            this.itemSet = [];
        }

    this.itemsModel.setProperty("/items", this.itemSet);
    this.itemsModel.refresh(true);


    },

    goBack: function (evt) {
        this.router.myNavBack("home");
    },
    
    
    exportToExcel: function(evt){
        var d = new Date();
        var data = d.toLocaleDateString();
        var time = d.toLocaleTimeString();
        var dataFile = data.concat(time);
        //var dataFile = utils.util.convertTimestamp(timestamp);
        //var dataFile = new Date().toISOString();
        var formatedDate= dataFile.replace(/[\/:]/g, "");
        var table = this.getView().byId("oTable");
        var exportDataTable = table.exportData();
        var exportType = exportDataTable.getAggregation("exportType");
        //exportType.setFileExtension("xls");
        //exportType.setMimeType("application/vnd.ms-excel");
        exportType.setSeparatorChar(";");
        exportType.applySettings();
        exportDataTable.saveFile("FILE_" + formatedDate).then(_.bind(function() {
           sap.m.MessageToast.show(this._getLocaleText("EXPORT_SUCCESSFUL"));
            var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
            oJQueryStorage.remove("itemSet_" + this.userID);
            var itemSetRefreshed = utils.LocalStorage.getItem("itemSet");
            this.itemsModel.setProperty("/items", itemSetRefreshed);
            this.itemsModel.refresh(true);
            var tab = this.getView().byId("oTable");
            //tab.refreshAggregation();
            //tab.refreshRows();
          
          }, this), _.bind(function(reason) {
            console.log(reason);
          sap.m.MessageToast.show(this._getLocaleText("EXPORT_FAILED"));
        }), this);
            
       
    },
    
    
    onDeleteRowPress: function(evt){
       
        var a = evt.getSource().getParent();
        var currentContext = a.oBindingContexts.items;
          var oModel = currentContext.getModel();
          var oData = oModel.getData();
          var currentRowIndex = parseInt(currentContext.getPath().split('/')[currentContext.getPath().split('/').length - 1]);
          var that = this;
          sap.m.MessageBox.show(this._getLocaleText("CONFIRM_DELETE_ROW"),{
			icon: sap.m.MessageBox.Icon.WARNING,
			title: this._getLocaleText("ALERT_CONFIRM_MESSAGEBOX_TITLE"),
            actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
		    onClose: function(response){
                if(response === sap.m.MessageBox.Action.YES){
                      oData.items.splice(currentRowIndex,1);
                      oModel.updateBindings();
                      that.refreshLocalModel();
                }else{
                    return;
                }
            }});
        
    },
    
    onEditRowPress: function(evt){
        var button = evt.getSource();
        var a = evt.getSource().getParent();
        var currentContext = a.oBindingContexts.items;
          var oModel = currentContext.getModel();
          //oModel.setProperty("/checked", false);
          var oData = oModel.getData();
          if(button.getText()==='EDIT'){
            button.setText('SAVE');
            oModel.setProperty('checked',true,currentContext);
            oModel.updateBindings();
            oModel.refresh(true);
            
          }else{
            button.setText('EDIT');
            oModel.setProperty('checked',false,currentContext);
            oModel.updateBindings();
            oModel.refresh(true);

          }
    },
    
    refreshLocalModel: function(){
        var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
        var arr = this.itemsModel.getData().items;
        oJQueryStorage.remove("itemSet_" + this.userID);
        oJQueryStorage.put("itemSet_" + this.userID, arr);
    },
    
     onPressLogOut: function (oEvent) {
        this._doLogout();
    },

    _doLogout: function () {
        //resetto dati appModel
        this.getView().getModel("appModel").setProperty("/isLogged", false);
        this.getView().getModel("appModel").setProperty("/entity", undefined);
        this.getView().getModel("appModel").setProperty("/username", "");
        this.getView().getModel("appModel").setProperty("/password", "");
        this.getView().getModel("appModel").setProperty("/storageArea", "");
        this.getView().getModel("appModel").setProperty("/userID", "");
        sessionStorage.clear();
        clearInterval(this.mySetInterval);
        this.router.myNavBack("");
        //this.router.destroy();
    }
    
   
//    exportToExcel: function(tableId, oModel)
//    {
//        var cols = tableId.getColumns();
//            var items = tableId.getRows();
//             var cellId = null;
//             var cellObj = null;
//             var cellVal = null;
//             var headerColId = null;
//             var headerColObj = null;
//             var headerColVal = null;
//             var column = null;
//             var json = {}; var colArray = []; var itemsArray = [];
//             //push header column names to array
//             for(var j=0; j<cols.length;j++){
//                    column = "";
//                    column = cols[j];
//                    headerColId = column.getAggregation("header").getId();
//                    headerColObj = sap.ui.getCore().byId(headerColId);
//                    headerColVal = headerColObj.getText();
//                    if(headerColObj.getVisible()){
//                        json={name: headerColVal};
//                        colArray.push(json);
//                    }
//                }
//                itemsArray.push(colArray);
//              //push table cell values to array
//              for (i = 0; i < items.length; i++) {
//                  colArray = [];
//                  cellId = "";   cellObj = "";  cellVal = "";
//                  headerColId = null; headerColObj = null; headerColVal = null;
//                  var item = items[i];
//                    for(var j=0; j<cols.length;j++){
//                        cellId = item.getAggregation("cells")[j].getId();
//                        cellObj = sap.ui.getCore().byId(cellId);
//                        if(cellObj.getVisible()){
//                            if(cellObj instanceof sap.m.Text ||cellObj instanceof sap.m.Label ||cellObj instanceof sap.m.Link) cellVal = cellObj.getText();
//                            if(cellObj instanceof sap.m.ObjectNumber){
//                                var k = cellObj.getUnit();
//                                cellVal = cellObj.getNumber()+" "+k;
//                            }
//                            if(cellObj instanceof sap.m.ObjectIdentifier){
//                                var objectIdentifierVal = "";
//                                if(cellObj.getTitle() != undefined && cellObj.getTitle() != "" && cellObj.getTitle() != null )
//                                    objectIdentifierVal = cellObj.getTitle();
//                                if(cellObj.getText() != undefined && cellObj.getText() != "" && cellObj.getText() != null )
//                                    objectIdentifierVal = objectIdentifierVal+" "+cellObj.getText();
//
//                                cellVal = objectIdentifierVal;
//                            }
//                            if(cellObj instanceof sap.ui.core.Icon){
//                                if(cellObj.getTooltip() != undefined && cellObj.getTooltip() != "" && cellObj.getTooltip() != null )
//                                cellVal = cellObj.getTooltip();
//                            }
//                            if(j==0){
//                                json={ name:  "\r"+cellVal};
//                            }
//                            else
//                            {
//                                json={ name:  cellVal};
//                            }
//                            colArray.push(json);
//                        }
//                    }
//                    itemsArray.push(colArray);
//
//
//                }
//             //export json array to csv file
//              var oExport = new sap.ui.core.util.Export({
//                    // Type that will be used to generate the content. Own ExportType's can be created to support other formats
//                    exportType: new sap.ui.core.util.ExportTypeCSV({
//                        separatorChar: ","
//                    }),
//                    // Pass in the model created above
//                    models: oModel,
//                    // binding information for the rows aggregation
//                    rows: {
//                        path: "/"
//                    },
//                    // column definitions with column name and binding info for the content
//                    columns: [itemsArray]
//                });
//              oExport.saveFile().always(function() {
//                    this.destroy();
//                });

//    }
    
    

//  refreshItemsTable : function(modelKey)
//  {
//
//
//    this.completeModel = model.Reports.getActivitiesModelByPeriod(modelKey);
//
//    // this.completeModel = model.Reports.getActivitiesWeekModel();
//    console.log(this.completeModel);
//
//
//    var activityForm = this.getView().byId("activitiesSummary");
//    if(this.completeList)
//    {
//      activityForm.removeContent(this.completeList);
//      delete(this.completeList);
//    }
//
//    this.completeList = new sap.m.List({});
//  //
//  var columns = this.completeModel.getData().columns;
//
//  //
//  //   //-----------To add date info: from-to------//
//  //
//  //   columns.unshift({"name":""});
//  //
//  //   //---------------------------------------------
//    for(var i = 0; i< columns.length; i++)
//    {
//      this.completeList.addColumn(new sap.m.Column({
//          hAlign: "Left",
//					vAlign: "Middle",
//
//					// minScreenWidth: "Tablet",
//					// demandPopin: true,
//					// popinDisplay:"Inline",
//					header: new sap.m.Label({
//						text: columns[i].name,
//						design: sap.m.LabelDesign.Bold
//					})
//      }));
//    }
//    var items = [];
//
//  //   var columnAggregations = this.completeList.getAggregation("columns");
//    var rows = this.completeModel.getData().rows;
//    for(var j=0; j< rows.length; j++)
//    {
//      var item = new sap.m.ColumnListItem({
//        width:"100%"
//      });
//      for(var k=0; k< rows[j].length; k++)
//      {
//        item.addCell(new sap.m.Text({
//          text:rows[j][k]
//       }));
//
//      }
//      this.completeList.addItem(item);
//    }
//    activityForm.addContent(this.completeList);
//
//  },













});
