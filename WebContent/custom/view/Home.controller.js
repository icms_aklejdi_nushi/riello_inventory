jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.LocalStorage");
//jQuery.sap.require("utils.util");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("utils.AbstractController");

utils.AbstractController.extend("view.Home", {

    onInit: function () {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
        
        this.enableModel = new sap.ui.model.json.JSONModel();
        this.enableModel.setProperty("/syncEnable", false);
        this.getView().setModel(this.enableModel, "enable");
        
        this.localModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.localModel, "localModel");
        
        this.testUserModel = new sap.ui.model.json.JSONModel(model.testUser);
        this.getView().setModel(this.testUserModel, "testUser");

        this.onlineModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.onlineModel, "online");
        
        this.inputModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.inputModel, "input");
        
        this.currentUserModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.currentUserModel, "currentUser");
        
        this.dateModel = new sap.ui.model.json.JSONModel();
        this.getView().setModel(this.dateModel, "date");
        
//        var rtData = {
//			"date" : new Date(),
//            "time" : new Date().toLocaleTimeString()
//		};
//        
//        this.dateModel.setData(rtData);



    },

    


    handleRouteMatched: function (evt) {
        
        var name = evt.getParameter("name");

        if (name !== "home") {
            return;
        };

        if (sessionStorage.getItem("userID")) {
            //sessionStorage.setItem("noRefresh", true);
            this.userID = sessionStorage.userID;
           
        };
        this.currentUserModel.setProperty("/userID", this.userID);
         this.currentUserModel.refresh(true);
        
        if(!sessionStorage.getItem("userID")){
            //this.router.navTo("");
            //this.router.destroy();
            this.currentUserModel.setProperty("/userID", "");
            this.currentUserModel.refresh(true);
        }
  

        this.itemSet = utils.LocalStorage.getItem("itemSet");
        if(this.itemSet === null || this.itemSet === undefined){
            this.itemSet = [];
        }

        this.localModel.setProperty("/items", this.itemSet);


        var that = this;
        var funzioneOnline = function () {
            Offline.options = {
                checks: {
                    xhr: {
                        url:'https://www.google.it'
                        //url: 'http://54.93.218.6:8000/IoT/app/RielloProject/WebContent/custom/image/favicon.ico'
                    }
                }
            };
            Offline.check();
            if (Offline.state === "up") {
                that.onlineModel.setProperty("/visibileOnline", false);
            } else if (Offline.state === "down") {
                that.onlineModel.setProperty("/visibileOnline", true);
            };
            that.onlineModel.refresh();
        };

        this.mySetInterval = setInterval(funzioneOnline, 30000);

        Offline.options = {
            checks: {
                xhr: {
                    url:'https://www.google.it'
                    //url: 'http://54.93.218.6:8000/IoT/app/RielloProject/WebContent/custom/image/favicon.ico'
                }
            }
        };
        Offline.check();
        if (Offline.state === "up") {
            this.onlineModel.setProperty("/visibileOnline", false);
        } else if (Offline.state === "down") {
            this.onlineModel.setProperty("/visibileOnline", true);
        };
        this.onlineModel.refresh();
    },
    
//    toggleStartStop: function(evt){
//        var recognizing = false;
//        var buttonText = this.getView().byId("microphone").mProperties.text; 
//        var recognition = new SpeechRecognition(); //webkitSpeechRecognition();
//        //recognition.continuous = true;
//        //if (buttonText === "Click to Start")
//        switch(buttonText){
//            case "Click to Start":{
//                recognition.start();
//                recognition.onresult = _.bind(function(evento) {
//                  if (evento.results.length > 0) {
//                      this.getView().byId("inpText").setValue(evento.results[0][0].transcript);
//                      this.getView().byId("microphone").setText("Click to Stop");
//                    //q.value = event.results[0][0].transcript;
//                    //q.form.submit();
//                  }
//                }, this)
//                
//                break;
//            }
//            case "Click to Stop":{
//                recognition.stop();
//                this.getView().byId("inpText").setValue("");
//                this.getView().byId("microphone").setText("Click to Start");
//                break;
//            }
//        }
//        
//    },


    
    deleteInput: function (evt) {
    var id = evt.getSource().getId().split("--")[1];
    //var sid;

    if (id === "delIDCode") {
            this.inputModel.setProperty("/code", "");
			sap.ui.getCore().byId(this.getView().getId()).byId("code").setValue("");
			

      //sid = this.CODE_ID;
    }
    else if(id === "delQuantity")
    {
      this.inputModel.setProperty("/quantity", "");
      sap.ui.getCore().byId(this.getView().getId()).byId("quantity").setValue("");
      //sid = this.CODE_ID;
    }
//    if (id === "delReadDate") {
//            this.inputModel.setProperty("/readDate", "");
//			sap.ui.getCore().byId(this.getView().getId()).byId("readDate").setValue("");
//			
//
//      //sid = this.CODE_ID;
//    }
//    if (id === "delReadTime") {
//            this.inputModel.setProperty("/readTime", "");
//			sap.ui.getCore().byId(this.getView().getId()).byId("readTime").setValue("");
//			
//
//      //sid = this.CODE_ID;
//    }
//    if (id === "delUser") {
//            this.inputModel.setProperty("/user", "");
//			sap.ui.getCore().byId(this.getView().getId()).byId("user").setValue("");
//			
//
//      //sid = this.CODE_ID;
//    }
//        if (sid) {
//      var ne = _.bind(this.setFocusOnElement, this, sid);
//      setTimeout(ne, 500);
//    }
  },
    
//    onSavePress: function(oEvent) {
//		sap.m.MessageBox.show(this._getLocaleText("CONFIRM"),
//			sap.m.MessageBox.Icon.QUESTION,
//			this._getLocaleText("CONFIRM_MESSAGEBOX_TITLE"), [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
//			_.bind(this.onLocalSavePress, this)
//		);
//	},
    
    //onLocalSavePress: function(evt){
    onSavePress: function(evt){
        var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
        var dataToSync = oJQueryStorage.get("itemSet_" + this.userID);
        //var items = this.localModel.getProperty("/items");
        var itemID = sap.ui.getCore().byId(this.getView().getId()).byId("code");
        var quantity = sap.ui.getCore().byId(this.getView().getId()).byId("quantity");
//        var readDate = sap.ui.getCore().byId(this.getView().getId()).byId("readDate");
//        var readTime = sap.ui.getCore().byId(this.getView().getId()).byId("readTime");
//        var user = sap.ui.getCore().byId(this.getView().getId()).byId("user");
        var d = new Date();
        var data = d.toLocaleDateString();
        var time = d.toLocaleTimeString();
        var checkID = itemID.getValue();
        var checkQuantity = quantity.getValue();
        if ((jQuery.trim(checkID)).length==0){
            
            sap.m.MessageToast.show(this._getLocaleText("NO_ID_TO_SAVE"), {
                title: "ERROR"
            });
            return;
        }
        if ((jQuery.trim(checkQuantity)).length==0){
            
            sap.m.MessageToast.show(this._getLocaleText("NO_QUANTITY_TO_SAVE"), {
                title: "ERROR"
            });
            return;
        }
//        if (evt === sap.m.MessageBox.Action.YES) {
            var obj = {};
            obj.itemID = itemID.getValue();
            obj.quantity = quantity.getValue();
            //obj.readDate = readDate.getValue();
            obj.readDate = data;
            //obj.readTime = readTime.getValue();
            obj.readTime = time;
            //obj.user = user.getValue();
            obj.user = this.userID;
            //items.push(obj);
            if (dataToSync) {
                dataToSync.push(obj);
            } else {
                dataToSync = [];
                dataToSync.push(obj);
            }
            oJQueryStorage.put("itemSet_" + this.userID, dataToSync);
            itemID.setValue("");
            quantity.setValue("");
//            readDate.setValue("");
//            readTime.setValue("");
//            user.setValue("");
            sap.m.MessageToast.show(this._getLocaleText("CHANGES_SAVED_ON_LOCAL"), {
                title: "SUCCESS"
            });
//        }else{
//            itemID.setValue("");
//            quantity.setValue("");
//            readDate.setValue("");
//            readTime.setValue("");
//            user.setValue("");
//            
//        }
        //this.router.navTo("home");
    },

    onChangeFocus: function(evt){
        var quant = this.getView().byId("quantity");
        quant.$().focus();
        var field = document.createElement('input');
        field.setAttribute('type', 'text');
        document.body.appendChild(field);

        setTimeout(function() {
            field.focus();
            setTimeout(function() {
                field.setAttribute('style', 'display:none;');
            }, 50);
        }, 50);

    },
    
    
    navToTable: function(){
//        this.router.navTo("detail", {
//            userID: this.userID
//        });
        this.router.myNavBack("detail");
    },



    onPressLogOut: function (oEvent) {
        this._doLogout();
    },

    _doLogout: function () {
        //resetto dati appModel
        this.getView().getModel("appModel").setProperty("/isLogged", false);
        this.getView().getModel("appModel").setProperty("/entity", undefined);
        this.getView().getModel("appModel").setProperty("/username", "");
        this.getView().getModel("appModel").setProperty("/password", "");
        this.getView().getModel("appModel").setProperty("/storageArea", "");
        this.getView().getModel("appModel").setProperty("/userID", "");
        sessionStorage.clear();
        clearInterval(this.mySetInterval);
        this.router.myNavBack("");
        //this.router.destroy();
    },
    
//    checkFields: function (evt) {
//        var obj = evt.getSource();
//        var val = obj.getValue();
//        if (!val || (jQuery.trim(val)).length==0) {
//            obj.setValueState("Error");
//            obj.setValueStateText(this._getLocaleText("BLANK_INPUT"));
//        } else {
//            obj.setValueState("None");
//        };
//    }



});
