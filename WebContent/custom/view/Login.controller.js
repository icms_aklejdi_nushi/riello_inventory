jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.testUser");
jQuery.sap.require("jquery.sap.storage");


sap.ui.controller("view.Login", {

    onExit: function () {

    },

    onInit: function () {

        this.router = sap.ui.core.UIComponent.getRouterFor(this);
        this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

        this.testUserModel = new sap.ui.model.json.JSONModel(model.testUser);
        this.getView().setModel(this.testUserModel, "testUser");

//        var oModel = new sap.ui.model.json.JSONModel(this._data);
//        this.getView().setModel(oModel);


    },
//    _data: {
//        "date": new Date()
//    },

    handleRouteMatched: function (evt) {

        var name = evt.getParameter("name");

        if (name !== "login") {
            return;
        };

        if (this._checkLogged()) {
            this.getView().getModel("appModel").setProperty("/tileSet", JSON.parse(sessionStorage.getItem("tileSet")));
            this.router.myNavBack("home");
        } else {
            document.onkeypress = _.bind(function (e) {
                if (e.keyCode == 13 && !_.isEmpty(this.getView().getModel("appModel").getProperty("/username")) &&
                    !_.isEmpty(this.getView().getModel("appModel").getProperty("/password")) && !_.isEmpty(this.getView().getModel("appModel").getProperty("/storageArea"))) {
                    this.onLoginPress();
                }
            }, this)
        }

        //this.oDialog = this.getView().byId("BusyDialog");
    },

    loadSavedCredentials: function () {
        // Controllo se ho un utente salvato
        var user = localStorage.getItem("user");
        if (user) {
            user = $.parseJSON(atob(user));
            this.username = user.user;
            this.password = user.pass;
            this.storageArea=user.sArea;
        } else {
            sap.m.MessageToast.show(
                this._getLocaleText("localUserUnavailable"), {
                    duration: 4000
                }
            );
            return;
        }
    },
    
    checkOffline: function () {
        Offline.options = {checks: {xhr: {url:'https://www.google.it'}}};
                                          //url:'http://54.93.218.6:8000/IoT/app/RielloProject/WebContent/custom/image/favicon.ico'
        Offline.check();
    },

    saveCredentials: function (user) {
        localStorage.setItem("user", btoa(JSON.stringify(user)));
        //defer.resolve(this.username);
    },


    onLoginPress: function (oEvent) {

        
        Offline.check();
        
        if(Offline.state === "up"){
            this.online = true;
        }
        else if (Offline.state === "down"){
            this.online = false;
        }
        


        if (_.isEmpty(this.getView().getModel("appModel").getProperty("/username")) ||
            _.isEmpty(this.getView().getModel("appModel").getProperty("/password")) ||
            _.isEmpty(this.getView().getModel("appModel").getProperty("/storageArea"))) {
            sap.m.MessageToast.show(
                this._getLocaleText("messageUserPasswordAreaEmpty"), {
                    duration: 4000
                }
            );
            return;
        }


        if (this.online === true) {

            //se logOG ===true {save credentials}
            this._doLogin(
                this.getView().getModel("appModel").getProperty("/username"),
                this.getView().getModel("appModel").getProperty("/password"),
                this.getView().getModel("appModel").getProperty("/storageArea")
            );


        }
        if (this.online === false) {
            this.loadSavedCredentials();

            
            if(this.getView().getModel("appModel").getProperty("/username")===this.username
               && this.getView().getModel("appModel").getProperty("/password")===this.password
               && this.getView().getModel("appModel").getProperty("/storageArea")===this.storageArea){
            this._doLogin(this.username,this.password, this.storageArea);
            }else{
               sap.m.MessageToast.show(
                    this._getLocaleText("usernotfound"), {
                        duration: 2000
                    }
                ); 
            }

        }

    },

    _doLogin: function (username, password, storageArea) {
        var localUser = {
            user: "",
            pass: "",
            sArea: ""
        };

        var logOk = false;
        var userOk = false;
        var passOk = false;
        var sAreaOk = false;
        var testUser = model.testUser.user;
        for (var i = 0; i < testUser.length; i++) {
            if (testUser[i].testUserID === username) {
                userOk = true;
                if (testUser[i].testUserPass === password) {
                    passOk = true;
                    if(testUser[i].storageArea === storageArea){
                    sAreaOk = true;
                    this.getView().getModel("appModel").setProperty("/isLogged", true);
                    sessionStorage.setItem("isLogged", true);
                    this.getView().getModel("appModel").setProperty("/userID", testUser[i].testUserID);
                    var userID = testUser[i].testUserID;

                    sessionStorage.setItem("userID", userID);

                    document.onkeypress = undefined;
                    logOk = true;
                        
                    break;
                    }
                }
                
            }
        }


        if (this.online === false && logOk === true) {

            this.router.myNavBack("home");
        }

        if (this.online === true && logOk === true) {
            //this.chiamateOdata();
            localUser.user = this.getView().getModel("appModel").getProperty("/username");
            localUser.pass = this.getView().getModel("appModel").getProperty("/password");
            localUser.sArea = this.getView().getModel("appModel").getProperty("/storageArea");
            this.saveCredentials(localUser);
            this.router.myNavBack("home");
        } else {
            if (userOk === false) {
                sap.m.MessageToast.show(
                    this._getLocaleText("usernotfound"), {
                        duration: 2000
                    }
                );
            } else 
                if (passOk === false){
                sap.m.MessageToast.show(
                    this._getLocaleText("passwordError"), {
                        duration: 2000
                    }
                );
            }  else 
                if (sAreaOk === false){
                sap.m.MessageToast.show(
                    this._getLocaleText("sAreaError"), {
                        duration: 2000
                    }
                );
            }

        }

        this.getView().getModel("appModel").setProperty("/username", "");
        this.getView().getModel("appModel").setProperty("/password", "");
        this.getView().getModel("appModel").setProperty("/storageArea", "");
    },

    _checkLogged: function () {
        return sessionStorage.getItem("isLogged");
    },

    _getLocaleText: function (key) {
        return this.getView().getModel("i18n").getProperty(key);
    },

//    dateToString: function (date, dataX) {
//        var selectedDate = new Date(date);
//
//        //manipolazione per giorno e mese
//        var day = selectedDate.getDate();
//        if (day < 10) {
//            day = "0" + day;
//        }
//        var month = selectedDate.getMonth() + 1;
//        if (month < 10) {
//            month = "0" + month;
//        }
//        var year = selectedDate.getFullYear();
//
//        if (dataX === "inDate") {
//            return selectedDate = year + "" + month + "" + day;
//        }
//        return selectedDate = day + "/" + month + "/" + year;
//        //fine manipolazione
//
//
//    },
    
    /*************

        syncroOnline: function(){
        var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
        var id = this.getView().getModel("appModel").getProperty("/ID");
        var cid = this.getView().getModel("appModel").getProperty("/CID");

        var attivitaSetLS = oJQueryStorage.get("attivitaSet_" + cid);
        var notaSpeseSetLS = oJQueryStorage.get("notaSpeseSet_" + cid);

        
        model.SyncronizeOnline.deferReports(attivitaSetLS, cid)

          .then(
            model.SyncronizeOnline.deferNotaSpese(notaSpeseSetLS, cid)
            .then(
                this.chiamateOdata()
            )
        )
        .catch(error);



//        $.ajax({
//            type: "POST",
//            url: 'http://something.xsjs',
//            dataType: "json",
//			//username: "",
//            //password: "",
//            //async : true,
//            success: function (res) {
//
//            sap.m.MessageToast.show("Rapportini sincronizzati correttamente!");
//
//            } ,
//            error: function (res){
//                   sap.m.MessageToast.show("Errore! Rapportini non sincronizzati!");
//            },
//            data: JSON.stringify(attivitaSetLS),
//            contentType:"application/json"
//            });
//
//        $.ajax({
//            type: "POST",
//            url: 'http://something.xsjs',
//            dataType: "json",
//			//username: "",
//            //password: "",
//            //async : true,
//            success: function (res) {
//
//            sap.m.MessageToast.show("Nota Spese Sincronizzata correttamente!");
//
//            } ,
//            error: function (res){
//                   sap.m.MessageToast.show("Errore! Nota spese non sincronizzata!");
//            },
//            data: JSON.stringify(notaSpeseSetLS),
//            contentType:"application/json"
//            });



    },
    
    ******/
    
    /************

    chiamateOdata() {
        this.oDialog.open();
        var oJQueryStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);

        var that = this;

        var id = this.getView().getModel("appModel").getProperty("/ID");
        var cid = this.getView().getModel("appModel").getProperty("/CID");

        var fError = function (err) {
            sap.m.MessageToast.show("Errore");
        };

        var fSuccessAttivitaSet = function (ris) {
            var attivitaSetOdata = _.chain(ris.d.results).map(function (el) {
                    return {
                        attivita: el.ATTIVITA,
                        color: el.COLOR,
                        commessa: el.COMMESSA,
                        dataCommessa: that.dateToString(new Date(parseInt(el.DATACOMMESSA.split("(")[1].split(")")[0]))),
                        durata: el.DURATA,
                        luogo: el.LUOGO,
                        nonFatturabile: el.NONFATTURABILE,
                        note: el.NOTE,
                        oreStraordinario: el.ORESTRAORDINARIO,
                        tipoStraordinario: el.TIPOSTRAORDINARIO,
                        userID: el.USERID,
                        stato: el.STATO
                    };
                })
                .value();
            var attivitaSetLS = oJQueryStorage.get("attivitaSet_" + cid);

            if (attivitaSetLS) {
                for (var i = 0; i < attivitaSetOdata.length; i++) {
                    var obj = _.find(attivitaSetLS, {
                        'dataCommessa': attivitaSetOdata[i].dataCommessa,
                        'commessa': attivitaSetOdata[i].commessa
                    });
                    if (!obj) {
                        attivitaSetLS.push(attivitaSetOdata[i]);
                    };
                };
                var attivitaSet = attivitaSetLS;
            } else {
                var attivitaSet = attivitaSetOdata;
            }
            oJQueryStorage.put("attivitaSet_" + cid, attivitaSet);

        };
        fSuccessAttivitaSet = _.bind(fSuccessAttivitaSet, this);

        $.ajax({
            type: "GET",
            url: "http://54.93.218.6:8000/IoT/odata/ftr.xsodata/attivitaSet?$filter=USERID eq '" + id + "'",
            success: fSuccessAttivitaSet,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });

        var fSuccessNotaSpeseSet = function (ris) {
            var notaSpeseSetOdata = _.chain(ris.d.results).map(function (el) {
                    return {
                        dataNotaSpese: that.dateToString(new Date(parseInt(el.DATANOTASPESE.split("(")[1].split(")")[0]))),
                        valuta: el.VALUTA,
                        commessa: el.COMMESSA,
                        categoriaSpesa: el.CATEGORIA,
                        tipoAuto: el.VEICOLO,
                        partenza: el.PARTENZA,
                        arrivo: el.ARRIVO,
                        importo: el.IMPORTO,
                        chilometri: el.CHILOMETRI,
                        note: el.NOTE,
                        userID: el.USERID,
                        stato: el.STATO
                    };
                })
                .value();

            var notaSpeseSetLS = oJQueryStorage.get("notaSpeseSet_" + cid);
            if (notaSpeseSetLS) {
                for (var i = 0; i < notaSpeseSetOdata.length; i++) {
                    var obj = _.find(notaSpeseSetLS, {
                        'dataNotaSpese': notaSpeseSetOdata[i].dataNotaSpese,
                        'commessa': notaSpeseSetOdata[i].commessa
                    });
                    if (!obj) {
                        notaSpeseSetLS.push(notaSpeseSetOdata[i]);
                    };
                };
                var notaSpeseSet = notaSpeseSetLS;
            } else {
                var notaSpeseSet = notaSpeseSetOdata;
            }
            oJQueryStorage.put("notaSpeseSet_" + cid, notaSpeseSet);
            this.oDialog.close();
            this.router.navTo("home");
        };
        fSuccessNotaSpeseSet = _.bind(fSuccessNotaSpeseSet, this);

        $.ajax({
            type: "GET",
            url: "http://54.93.218.6:8000/IoT/odata/ftr.xsodata/notaspeseSet?$filter=USERID eq '" + id + "'",
            success: fSuccessNotaSpeseSet,
            error: fError,
            dataType: "json",
            contentType: "application/json"
        });
    }
    
    */

});
