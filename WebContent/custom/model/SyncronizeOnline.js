jQuery.sap.declare("model.SyncronizeOnline");

model.SyncronizeOnline = {
    
url: "http://54.93.218.6:8000/IoT/odata/ftr.xsodata",
    
getODataModel: function () {
        if (this._oDataModel === undefined) {
            this._oDataModel = new sap.ui.model.odata.ODataModel(this.url, true);
        }
        return this._oDataModel;
},
    
deferReports: function (reports, user) {
        var defer = Q.defer();

        // Imposto l'oggetto
        var e = {};
        e.reports = reports;
        e.user = user;
        this.syncronizeReports(e, defer.resolve, defer.reject);

        return defer.promise;
},
    
deferNotaSpese: function (notaSpese, user) {
        var defer = Q.defer();

        // Imposto l'oggetto
        var e = {};
        e.notaSpese = notaSpese;
        e.user = user;
        this.syncronizeNotaSpese(e, defer.resolve, defer.reject);

        return defer.promise;
},

syncronizeReports: function(reports, success, error)
    {
        var params = {
            async: false,
            success: success,
            error: error,

        };


        this.getODataModel().create(
            "/attivitaSet",
            reports,
            params
        );

    },
    
syncronizeNotaSpese: function(notaSpese, success, error)
    {
        var params = {
            async: false,
            success: success,
            error: error,

        };


        this.getODataModel().create(
            "/notaSpeseSet",
            notaSpese,
            params
        );

    }
    
};