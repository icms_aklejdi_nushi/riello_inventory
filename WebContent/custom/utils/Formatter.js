jQuery.sap.declare("utils.Formatter");

utils.Formatter = {

    not: function (value) {
        return !value;
    },
    
    formatDateToPattern: function (date, pattern) {
        var format = sap.ui.core.format.DateFormat.getInstance({
            pattern: pattern
        });
        return format.format(date);
    },

    color: function (value) {
        if (value === undefined || value === 0) {
            return "#000000"
        } else if (value === 1) {
            return "#00FF00";
        };
    },


    trasformaTestoOnline: function (state) {
        if (state) {
            if (state === true) {
                return "ONLINE";
            } else {
                return "OFFLINE";
            };
        };
    },

    trasformaColoreOnline: function (state) {
        if (state) {
            if (state === true) {
                return "Accept";
            } else {
                return "Reject";
            };
        };
    },
    checkDeleteSize: function(isNoPhone){
        if(isNoPhone){
            return "8%";
        }else{return "auto";}
    },
    
    checkIDifPhone: function(isNoPhone){
        if(isNoPhone){
           return "23%"; 
        }else{return "auto";}
    },
    checkUserIDifPhone: function(isNoPhone){
        if(isNoPhone){
            return "23%";
        }else{return "auto";}
    },
    checkQuantityIfPhone: function(isNoPhone){
        if(isNoPhone){
            return "6%";
        }else{return "auto"}
    },
    checkReadDataIfPhone: function(isNoPhone){
        if(isNoPhone){
            return "20%";
        }else{return "auto";}
    },
    checkReadTimeIfPhone: function(isNoPhone){
        if(isNoPhone){
            return "20%";
        }else{return "auto";}
    }
    

};
