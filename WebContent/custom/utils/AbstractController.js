jQuery.sap.declare("utils.AbstractController");

sap.ui.core.mvc.Controller.extend("utils.AbstractController", {

    onInit: function () {
//        this._data = {
//            "date": new Date()
//        };
//        var oModel = new sap.ui.model.json.JSONModel(this._data);
//        this.getView().setModel(oModel);
    },
    
    getRouter : function () {
		return sap.ui.core.UIComponent.getRouterFor(this);
	},


    _getLocaleText: function (key) {
        return this.getView().getModel("i18n").getProperty(key);
    }


});
