jQuery.sap.declare("utils.util");

utils.util = {


  stringToDate: function(sDateGGMMAAAA)
  {
    var objData = new Date();
    objData.setDate(sDateGGMMAAAA.substr(0, 2));
    var month = parseInt(sDateGGMMAAAA.substr(3, 2));
    objData.setMonth(month-1);
    objData.setFullYear(sDateGGMMAAAA.substr(6));
    return objData;
  },

	compareDate:function(date1, date2)
	{
		date1= new Date(date1);
		date2 = new Date(date2);
		if(date1.getFullYear() !== date2.getFullYear())
			return false;
		if(date1.getMonth() !== date2.getMonth())
			return false;
		if(date1.getDate()!== date2.getDate())
			return false;
		else {
			return true;
		}
	},


    formatDate : function(date){
    var d = date;
    var gg = d.getDate();
    var mm = d.getMonth()+1;
    var yyyy = d.getFullYear();
    if(gg<10){
    gg = "0"+gg;
    }
    if(mm<10){
    mm="0"+mm;
    }
    return gg+"/"+mm+"/"+yyyy;

  },
    
    convertTimestamp: function(timestamp) {
      var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
            dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh == 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM	
        time = yyyy + '_' + mm + '_' + dd + 'T ' + h + ':' + min + ' ' + ampm;

        return time;
    }


};
